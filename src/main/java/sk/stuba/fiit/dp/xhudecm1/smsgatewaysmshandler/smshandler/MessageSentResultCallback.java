/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler;

import java.util.UUID;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutAbstract;

/**
 *
 * @author martinhudec
 */
public interface MessageSentResultCallback {

    public void messageSentResult(UUID messageUUID, Integer messageReferenceNumber, MessageSentResultEnum result, ModemInstance modem);

    public enum MessageSentResultEnum {
        OK, ERROR
    }

}
 