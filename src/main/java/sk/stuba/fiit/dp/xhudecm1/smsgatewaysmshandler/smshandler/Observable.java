/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler;

/**
 *
 * @author martinhudec
 */
public interface Observable {

    public void addObserver(NewMessageObserver observer);

    public void removeObserver(NewMessageObserver observer);

    public void notifyNewMessageObserver();

    public void notifyNewStatusReportMessageObserver();
}
