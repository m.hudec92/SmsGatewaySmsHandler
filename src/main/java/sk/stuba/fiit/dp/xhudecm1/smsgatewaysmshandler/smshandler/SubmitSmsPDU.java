/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler;

import java.util.UUID;

/**
 *
 * @author martinhudec
 */
public class SubmitSmsPDU {

    String destinationNumber;
    byte[] payload;
    SmsSecurePDU securePdu;
    private final UUID messageUUID;


    public SubmitSmsPDU(String number, byte[] payload) {
        this.destinationNumber = number;
        this.payload = payload;
        this.messageUUID = UUID.randomUUID();
    }

    public SubmitSmsPDU(String number, SmsSecurePDU securePdu) {
        this.destinationNumber = number;
        this.securePdu = securePdu;
        this.messageUUID = UUID.randomUUID();
    }

    public String getDestinationNumber() {
        return destinationNumber;
    }

    public byte[] getPayload() {
        if (securePdu != null) {
            return securePdu.generatePdu();
        } else {
            return payload;
        }
    }

    public UUID getMessageUUID() {
        return messageUUID;
    }

}
