/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UnableToLoadModemConfiguration;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.SerialGsmModemCommunicatorException;

/**
 *
 * @author martinhudec
 */
public abstract class SmsHandler implements SmsHandlerInterface, NewMessageObserver, Runnable {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SmsHandler.class);
    protected List<ModemInstance> modemList = new CopyOnWriteArrayList<>();
    private List<ModemInstance> receivedMessagesModemList = null;
    private List<ModemInstance> receivedStatusReportMessageModemList = null;

    public SmsHandler() throws SerialGsmModemCommunicatorException, UnableToLoadModemConfiguration {
        receivedMessagesModemList = new CopyOnWriteArrayList<>();
        receivedStatusReportMessageModemList = new CopyOnWriteArrayList<>();
        initModems();
    }

    private void initModems() throws SerialGsmModemCommunicatorException, UnableToLoadModemConfiguration {
        specifyModems();
        logger.debug("INITIALIZING MODEMS " + modemList.size());
        try {
            for (ModemInstance modemInstance : modemList) {
                modemInstance.addObserver(this);
            }

        } catch (Exception ex) {
            logger.error("Unable to initialize modem", ex);
        }
        new Thread(this).start();
 
    }

    protected abstract void specifyModems() throws SerialGsmModemCommunicatorException, UnableToLoadModemConfiguration;

    protected abstract ModemInstance getModem(SubmitSmsPDU pdu);

    protected abstract void processReceivedMessage(DeliverSmsPDU receivedMessage);

    protected abstract void processReceivedDeliveryStatusMessage(DeliveryStatusSmsPDU deliveryStatusMessage);
    
    
    @Override
    public void sendMessage(SubmitSmsPDU pdu) {
        logger.info("Sending new message with uuid "+ pdu.getMessageUUID() + " to " + pdu.getDestinationNumber());
        getModem(pdu).addMessageToSendQueue(pdu);
    }

    @Override
    public void newMessageNotification(ModemInstance modemInstance) {
        logger.debug("NEW MESSAGE NOTIFICATION");
        logger.info("modemListNotificationSize " + receivedMessagesModemList.size());
        logger.info(modemInstance.getModemSerialPort() + "new message");
        if (!receivedMessagesModemList.contains(modemInstance)) {
            logger.debug("adding new modem notification");
            receivedMessagesModemList.add(modemInstance);
        }
    }

    @Override
    public void newStatusReportMessageNotification(ModemInstance modemInstance) {
        if (!receivedStatusReportMessageModemList.contains(modemInstance)) {
            receivedStatusReportMessageModemList.add(modemInstance);
        }
    }

    @Override
    public void run() {
        while (true) {
            if (!receivedMessagesModemList.isEmpty()) {
                logger.info("received new message notification");
                       
                Random rnd = new Random();
                Integer nextModemNumber = rnd.nextInt(receivedMessagesModemList.size());
                if (nextModemNumber > 0) {
                    nextModemNumber--;
                }
                ModemInstance modem = receivedMessagesModemList.get(nextModemNumber);

                DeliverSmsPDU receivedMessage = modem.getReceivedMessagesQueue().poll();
                logger.debug("RECEIVED message " + receivedMessage.getPayloadHexString() + " " + receivedMessage.getOriginatorNumber());
                processReceivedMessage(receivedMessage);
                if (modem.getReceivedMessagesQueue().isEmpty()) {
                    receivedMessagesModemList.remove(modem);
                    logger.debug("received message queue is empty for modem " + modem.getModemSerialPort());
                    logger.debug("RECEIVED MESSAGES MODEM LIST is empty = " + receivedMessagesModemList.isEmpty());

                }

            } else if (!receivedStatusReportMessageModemList.isEmpty()) {
                Random rnd = new Random();
                Integer nextModemNumber = rnd.nextInt(receivedStatusReportMessageModemList.size());
                if (nextModemNumber > 0) {
                    nextModemNumber--;
                }
                ModemInstance modem = receivedStatusReportMessageModemList.get(nextModemNumber);
                DeliveryStatusSmsPDU receivedMessage = modem.getReceivedStatusReportQueue().poll();
                logger.info("RECEIVED Status report " + receivedMessage.getPayloadHexString() + " " + receivedMessage.getOriginatorNumber());
                processReceivedDeliveryStatusMessage(receivedMessage);
                if (modem.getReceivedStatusReportQueue().isEmpty()) {
                    receivedStatusReportMessageModemList.remove(modem);
                    logger.debug("received statur report message queue is empty for modem " + modem.getModemSerialPort());
                    logger.debug("RECEIVED MESSAGES MODEM LIST is empty = " + receivedStatusReportMessageModemList.isEmpty());

                }
            } else {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ex) {
                    throw new RuntimeException(ex);
                    
                }
            }
        }
    }

    @Override
    public void registerMessageSentResultCallback(MessageSentResultCallback callback) {
        for (ModemInstance modemInstance : modemList) {
            modemInstance.addMessageSentResultCallback(callback);
        }
    }

}
