/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import org.apache.commons.lang3.ArrayUtils;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UknownPduConentType;

/**
 *
 * @author martinhudec
 */
public abstract class SmsSecurePDU {

    public static SmsSecurePDU createNewSecurePdu(byte[] payload) throws UknownPduConentType {
        byte[] secureHeader = Arrays.copyOfRange(payload, 0, 4);
        logger.debug("Secure header " + Utils.toHexString(secureHeader));
        Integer headerSum = getBitFromHeader(new BigInteger(secureHeader).intValue(), 0, 16) % 32767;
        logger.info("header sum " + headerSum);

        Integer headerCheckSum = getBitFromHeader(new BigInteger(secureHeader).intValue(), 17, 31);
        logger.info("header checksum " + headerCheckSum);
        logger.info("header" + Utils.toHexString(secureHeader));
        if (!(headerSum).equals(headerCheckSum)) {

            logger.error("header sum " + headerSum + " header checksum " + headerCheckSum);
            throw new UknownPduConentType("Received message with unknown PDU content Type header checksum MISMATCH");
        }

        ContentTypeEnum contentType = ContentTypeEnum.values()[getBitFromHeader(new BigInteger(secureHeader).intValue(), 8, 9)];
        logger.debug("Creating new securePDU object for content type " + contentType);
        switch (contentType) {
            case DATA:
                return new SmsSecurePDUData(payload);
            case KEY:
                return new SmsSecurePDUKey(payload);
            case TECHNICAL:
                return new SmsSecurePDUTechnical(payload);
            default:
                throw new UknownPduConentType("Received message with unknown PDU content Type");
        }
    }

    protected Integer sequenceNumber;
    protected ContentTypeEnum contentType;
    protected CypherActiveEnum cypherActive;
    protected byte[] securePayload;
    protected byte[] decryptedPayload;
    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SmsSecurePDU.class);

    public SmsSecurePDU(Integer sequenceNumber, ContentTypeEnum contentType, CypherActiveEnum cypherActive, byte[] securePayload) {
        this.sequenceNumber = sequenceNumber;
        this.contentType = contentType;
        this.cypherActive = cypherActive;
        this.securePayload = securePayload;
    }

    protected SmsSecurePDU(byte[] payload) {
        byte[] secureHeader = Arrays.copyOfRange(payload, 0, 4);
        logger.debug("PARSING SECURE HEADER " + Utils.toHexString(secureHeader));
        getHeaderValues(new BigInteger(secureHeader).intValue());
        setSecurePayload(Arrays.copyOfRange(payload, 4, payload.length));
    }

    protected abstract void getHeaderValuesSpecific(Integer commonHeader);

    private void getHeaderValues(Integer header) {
        setSequenceNumber(getBitFromHeader(header, 0, 7));
        setContentType(ContentTypeEnum.values()[getBitFromHeader(header, 8, 9)]);
        setCypherActive(CypherActiveEnum.values()[getBitFromHeader(header, 10, 10)]);
        getHeaderValuesSpecific(header);
    }

    public byte[] generatePdu() {
        byte[] secureHeader = setHeader();
        byte[] securePdu = ArrayUtils.addAll(secureHeader, securePayload);
        return securePdu;
    }

    private Integer getCommonHeaderValue() {
        Integer header = 0;
        header = setBitInHeader(header, 0, 7, sequenceNumber); //seq
        header = setBitInHeader(header, 8, 9, contentType.getNumVal()); // contentID
        header = setBitInHeader(header, 10, 10, cypherActive.getNumVal()); // cypher on off 
        return header;
    }

    protected abstract Integer setHeaderSpecific(Integer commonHeader);

    private byte[] setHeader() {
        Integer header = 0;
        header = getCommonHeaderValue();
        header = setHeaderSpecific(header);
        logger.debug("generating header checksum from header " + header);
        logger.debug("header checksum " + (header % 32767));
        header = setBitInHeader(header, 17, 31, header % 32767); // checksum 

        String headerHexString = Integer.toHexString(header);
        // musime prirtat na zaciatok 0 pokial to ma neparny pocet znakov
        logger.debug("SETTING HEADER seq num: " + sequenceNumber + " original header string " + headerHexString);
        if (sequenceNumber < 15) {
            logger.debug("adding 0 to start - > sequence number was lower than 4 bit value");
            headerHexString = "0" + headerHexString;
        }

        logger.debug("GENEREATED HEADER " + header + " " + headerHexString);
        return Utils.toByteArray(headerHexString);
    }

    protected static Integer getBitFromHeader(Integer header, int startIndex, int endIndex) {
        Integer length = (endIndex - startIndex) + 1;
        Integer shift = 32 - (endIndex + 1);
        Integer workPayload = header >> shift;
        logger.debug(workPayload);
        workPayload &= bitValue(length);
        logger.debug("PARSING INTEGER FROM HEADER from " + startIndex + " -> " + endIndex);
        logger.debug("PARSED INTEGER FROM HEADER " + workPayload);
        return workPayload;
    }

    protected static Integer bitValue(Integer length) {
        Integer value = 1;
        for (int i = 1; i <= length; i++) {
            value *= 2;
        }
        logger.debug("BIT VALUE " + (value - 1));
        return value - 1;
    }

    protected static Integer setBitInHeader(Integer header, int startIndex, int endIndex, Integer value) {
        Integer length = (endIndex - startIndex) + 1;
        header <<= length;
        header |= value | (0 << length);
        String headerHexString = Integer.toHexString(header);
        logger.debug("CREATED BYTE " + headerHexString + " int value " + header + " set bit start " 
                + startIndex + " end " + endIndex + " value " + value);
        return header;
    }

    public void setContentType(ContentTypeEnum contentType) {

        this.contentType = contentType;
    }

    public void setCypherActive(CypherActiveEnum cypherActive) {
        this.cypherActive = cypherActive;
    }

    private void setSecurePayload(byte[] securePayload) {
        logger.debug("setting secure payload " + Utils.toHexString(securePayload));
        this.securePayload = securePayload;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public ContentTypeEnum getContentType() {
        return contentType;
    }

    public CypherActiveEnum getCypherActive() {
        return cypherActive;
    }

    public byte[] getPayload() {
        return securePayload;
    }

    public void setDecryptedPayload(byte[] decryptedPayload) {
        this.decryptedPayload = decryptedPayload;
    }

    public byte[] getDecryptedPayload() {
        return decryptedPayload;
    }

    public byte[] getPureData() {
        logger.debug("RETURNING DECRYPTED PAYLOAD AS PURE DATA");
        return decryptedPayload;
    }

    public enum ContentTypeEnum {
        DATA(0), KEY(1), TECHNICAL(2);

        private int numVal;

        private ContentTypeEnum(int numVal) {
            logger.info("setting content type " + numVal);
            this.numVal = numVal;
        }

        public int getNumVal() {
            return numVal;
        }

    }

    public enum CompressionTypeEnum {
        SHOCO(0), SMAZ(1), CUSTOM_ALPHABET(2), DEFLATER(3), NONE(4), SHOCO_DEFLATER(5), SMAZ_DEFLATER(6);

        private int numVal;

        private CompressionTypeEnum(int numVal) {
            this.numVal = numVal;
        }

        public int getNumVal() {
            return numVal;
        }

    }

    public enum KeyExchangeStageEnum {
        ACK(0), REQUEST(1), RESPONSE(2), API_KEY_REQUEST(3), API_KEY_ACK_SUCCESS(4), API_KEY_ACK_ERROR(5);
        private int numVal;

        private KeyExchangeStageEnum(int numVal) {
            this.numVal = numVal;
        }

        public int getNumVal() {
            return numVal;
        }

    }

    public enum TechnicalMessageTypeEnum {
        PING(0), REQUEST_KEY_EXCHANGE(1);
        private int numVal;

        private TechnicalMessageTypeEnum(int numVal) {
            this.numVal = numVal;
        }

        public int getNumVal() {
            return numVal;
        }

    }

    public enum CypherActiveEnum {
        ON(0), OFF(1);
        private int numVal;

        private CypherActiveEnum(int numVal) {
            this.numVal = numVal;
        }

        public int getNumVal() {
            return numVal;
        }

    }

    public enum DestinationTypeEnum {
        WS(0), EMAIL(1), SMS(2), STORE(3), DEFAULT(4);
        private int numVal;

        private DestinationTypeEnum(int numVal) {
            this.numVal = numVal;
        }

        public int getNumVal() {
            return numVal;
        }

    }

    @Override
    public String toString() {
        return " Sequence number " + sequenceNumber + " contentType " + contentType
                + " cypherActiveEnum " + cypherActive + " securePayload " + Utils.toHexString(securePayload);
    }

}
