/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums;

/**
 *
 * @author martinhudec
 */
public enum SmsSentStatusType {
    SMS_SENT,
    SMS_SENT_ACK_RECEIVED,
    SMS_SENT_ERROR,
    SMS_SENT_DELIVERED,
    SMS_SENT_REPEATING_DELIVERY,
    SMS_SENT_UNABLE_TO_DELIVER
}
