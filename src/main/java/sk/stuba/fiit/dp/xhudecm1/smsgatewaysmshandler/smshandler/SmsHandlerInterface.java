/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler;



/**
 *
 * @author martinhudec
 */
public interface SmsHandlerInterface {

    public void sendMessage(SubmitSmsPDU smsPdu);
    public void registerMessageSentResultCallback(MessageSentResultCallback callback);
}
