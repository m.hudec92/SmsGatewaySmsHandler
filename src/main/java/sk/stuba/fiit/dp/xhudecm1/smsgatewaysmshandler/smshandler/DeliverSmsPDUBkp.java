/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler;

import java.util.UUID;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;
import sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions.UknownPduConentType;

/**
 *
 * @author martinhudec
 */
public class DeliverSmsPDUBkp {

    private final String originatorNumber;
    private final String originatorNumberType;
    private final String originatorNumberingPlan;
    private final String smscNumber;
    private final String smscNumberType;
    private final String smscNumberingPlan;
    private final String timestamp;
    private final String payloadHexString;
    private final UUID messageUUID;
    private byte[] payload;
    private final ModemInstance modemInstance;
    private SmsSecurePDU securePdu;

    public DeliverSmsPDUBkp(String originatorNumber, String originatorNumberType, String originatorNumberingPlan, String smscNumber, String smscNumberType, String smscNumberingPlan, String timestamp, String payloadHexString, ModemInstance modemInstance) {
        this.originatorNumber = originatorNumber;
        this.originatorNumberType = originatorNumberType;
        this.originatorNumberingPlan = originatorNumberingPlan;
        this.smscNumber = smscNumber;
        this.smscNumberType = smscNumberType;
        this.smscNumberingPlan = smscNumberingPlan;
        this.timestamp = timestamp;
        this.payloadHexString = payloadHexString;
        this.modemInstance = modemInstance;
        this.messageUUID = UUID.randomUUID();
        this.payload = Utils.toByteArray(payloadHexString);
    }

    public String getOriginatorNumber() {
        return originatorNumber;
    }

    public String getOriginatorNumberType() {
        return originatorNumberType;
    }

    public String getOriginatorNumberingPlan() {
        return originatorNumberingPlan;
    }

    public String getSmscNumber() {
        return smscNumber;
    }

    public String getSmscNumberType() {
        return smscNumberType;
    }

    public String getSmscNumberingPlan() {
        return smscNumberingPlan;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getPayloadHexString() {
        return payloadHexString;
    }

    public byte[] getPayload() {
        return payload;
    }

    public UUID getMessageUUID() {
        return messageUUID;
    }

    public ModemInstance getModemInstance() {
        return modemInstance;
    }

    public void setSecurePdu(SmsSecurePDU securePdu) {
        this.securePdu = securePdu;
    }

    public void checkValidSecurePdu() throws UknownPduConentType {
        if (securePdu == null) {
            this.securePdu = SmsSecurePDU.createNewSecurePdu(payload);
        }
    }

    public SmsSecurePDU getSecurePdu() throws UknownPduConentType {
        if (securePdu == null) {
            this.securePdu = SmsSecurePDU.createNewSecurePdu(payload);
        }
        return securePdu;
    }

}
