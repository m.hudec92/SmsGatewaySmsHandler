/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler;

/**
 *
 * @author martinhudec
 */
public class SmsSecurePDUTechnical extends SmsSecurePDU {

    private CompressionTypeEnum compressionType;
    private TechnicalMessageTypeEnum technicalMessageType;
    protected byte[] decompressedPayload;
    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SmsSecurePDUTechnical.class);

    public SmsSecurePDUTechnical(Integer sequenceNumber, ContentTypeEnum contentType, CypherActiveEnum cypherActive, CompressionTypeEnum compressionType, TechnicalMessageTypeEnum technicalMessageType, byte[] securePayload) {
        super(sequenceNumber, contentType, cypherActive, securePayload);
        this.compressionType = compressionType;
        this.technicalMessageType = technicalMessageType;
    }

    public SmsSecurePDUTechnical(byte[] payload) {
        super(payload);
    }

    @Override
    protected Integer setHeaderSpecific(Integer commonHeader) {
        Integer header = commonHeader;
        header = setBitInHeader(header, 11, 13, technicalMessageType.getNumVal()); //tech mess type
        header = setBitInHeader(header, 14, 16, compressionType.getNumVal()); // compress type
        //header = setBitInHeader(header, 17, 23, 127); // alphabed ident
        return header;

    }

    @Override
    protected void getHeaderValuesSpecific(Integer commonHeader) {
        setTechnicalMessageType(TechnicalMessageTypeEnum.values()[getBitFromHeader(commonHeader, 11, 13)]);
        setCompressionType(CompressionTypeEnum.values()[getBitFromHeader(commonHeader, 14, 16)]);
        //setContentType(ContentTypeEnum.values()[getBitFromHeader(header, 8, 9)]);
    }

    public CompressionTypeEnum getCompressionType() {
        return compressionType;
    }

    public void setCompressionType(CompressionTypeEnum compressionType) {
        this.compressionType = compressionType;
    }

    public TechnicalMessageTypeEnum getTechnicalMessageType() {
        return technicalMessageType;
    }

    public void setTechnicalMessageType(TechnicalMessageTypeEnum technicalMessageType) {
        this.technicalMessageType = technicalMessageType;
    }

    @Override
    public String toString() {
        return "technicalMessageType " + technicalMessageType + " compressionType " + compressionType + super.toString();
    }

    public byte[] getDecompressedPayload() {
        return decompressedPayload;
    }

    public void setDecompressedPayload(byte[] decompressedPayload) {
        this.decompressedPayload = decompressedPayload;
    }

    @Override
    public byte[] getPureData() {
        if (compressionType != CompressionTypeEnum.NONE) {
            logger.debug("Returning decompressed Payload");
            return decompressedPayload;
        } else {
            logger.debug("returning decryptedPayload");
            return decryptedPayload;
        }
    }

}
