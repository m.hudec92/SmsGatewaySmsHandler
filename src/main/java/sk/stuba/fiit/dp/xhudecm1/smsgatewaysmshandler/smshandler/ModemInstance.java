/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutAbstract;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCMGS;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ReceivedMessageDeliveryStatusInterface;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ReceivedMessageInterface;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.MessageDeliveryStatusCallback;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsPDU;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsPDUStatusReport;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;

import static sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.SmsHandler.logger;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandErrorException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotSupportedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ErrorWritingATCommandToSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.InvalidAtCommandInputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.NoResponseReadFromSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.SerialGsmModemCommunicatorException;

/**
 *
 * @author martinhudec
 */
public class ModemInstance implements ModemInstanceInterface, Observable {

    private final String modemSerialPort;
    private final String modemStorage;
    private final String simCardPin;
    private final String mobileOperator;
    private final Modem modem;
    private final Queue<SubmitSmsPDU> messagesToBeSentQueue;
    private final ConcurrentLinkedQueue<DeliverSmsPDU> receivedMessagesQueue;
    private final ConcurrentLinkedQueue<DeliveryStatusSmsPDU> receivedStatusReportQueue;
    private final List<NewMessageObserver> smsHandlerList = new ArrayList<>();
    private boolean newMessage = false;
    private final List<MessageSentResultCallback> callbackList;
    private Long hitCount = 0l;

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ModemInstance.class);

    public ModemInstance(String modemSerialPort, String modemStorage, String simCardPin, String mobileOperator) throws SerialGsmModemCommunicatorException {
        this.modemSerialPort = modemSerialPort;
        this.modemStorage = modemStorage;
        this.simCardPin = simCardPin;
        this.mobileOperator = mobileOperator;
        this.callbackList = new CopyOnWriteArrayList<>();
        this.messagesToBeSentQueue = new LinkedList<>();
        this.receivedMessagesQueue = new ConcurrentLinkedQueue<>();
        this.receivedStatusReportQueue = new ConcurrentLinkedQueue<>();
        modem = new Modem(modemSerialPort, modemStorage);
        initModemCallBacks();
        new Thread(new MessageSenderThread(this)).start();
    }

    private void initModemCallBacks() {
        modem.setMessageReceivedCallback((ReceivedMessageInterface receivedMessage) -> {
            SmsPDU pdu = receivedMessage.getMessagePdu();
            receivedMessagesQueue.add(new DeliverSmsPDU(
                    pdu.getSenderAddress().getNumber(),
                    pdu.getSenderAddress().getNumberType().toString(),
                    pdu.getSenderAddress().getNumberingPlan().toString(),
                    pdu.getSmscAddress().getNumber(),
                    pdu.getSmscAddress().getNumberType().toString(),
                    pdu.getSmscAddress().getNumberingPlan().toString(),
                    pdu.getSmsTimeStamp().toString(),
                    pdu.getSmsDataPayloadHexString(), this));
            newMessage = true;
            notifyNewMessageObserver();
        });
        modem.setMessageDeliveryStatusCallback((ReceivedMessageDeliveryStatusInterface receivedDeliveryStatus) -> {
            SmsPDUStatusReport statusReport = receivedDeliveryStatus.getStatusReport();
            receivedStatusReportQueue.add(new DeliveryStatusSmsPDU(
                    statusReport.getSenderAddress().getNumber(),
                    statusReport.getSenderAddress().getNumberType().toString(),
                    statusReport.getSenderAddress().getNumberingPlan().toString(),
                    statusReport.getSmscAddress().getNumber(),
                    statusReport.getSmscAddress().getNumberType().toString(),
                    statusReport.getSmscAddress().getNumberingPlan().toString(),
                    statusReport.getSmsTimeStamp().toString(),
                    statusReport.getSmsPduHexString(),
                    this,
                    statusReport.getReferenceNumber(),
                    statusReport.getDeliveryStatus()));
            newMessage = true;
            notifyNewStatusReportMessageObserver();
        });
    }

    public String getModemSerialPort() {
        return modemSerialPort;
    }

    public String getModemStorage() {
        return modemStorage;
    }

    public String getSimCardPin() {
        return simCardPin;
    }

    public String getMobileOperator() {
        return mobileOperator;
    }

    public Modem getModem() {
        return modem;
    }

    @Override
    public void addMessageToSendQueue(SubmitSmsPDU pdu) {
        logger.debug("-----------------------__>adding new smsm to queue queue size " + messagesToBeSentQueue.size());
        messagesToBeSentQueue.add(pdu);
    }

    @Override
    public void addObserver(NewMessageObserver observer) {
        smsHandlerList.add(observer);
    }

    @Override
    public void removeObserver(NewMessageObserver observer) {
        smsHandlerList.remove(observer);
    }

    @Override
    public void notifyNewMessageObserver() {
        smsHandlerList.forEach((smsHandler) -> {
            smsHandler.newMessageNotification(this);
        });
    }

    @Override
    public void notifyNewStatusReportMessageObserver() {
        for (NewMessageObserver smsHandler : smsHandlerList) {
            smsHandler.newStatusReportMessageNotification(this);
        }
    }

    @Override
    public Queue<DeliverSmsPDU> getReceivedMessagesQueue() {
        return receivedMessagesQueue;
    }

    @Override
    public Queue<DeliveryStatusSmsPDU> getReceivedStatusReportQueue() {
        return receivedStatusReportQueue;
    }

    @Override
    public boolean isNewMessage() {
        return newMessage;
    }

    @Override
    public void addMessageSentResultCallback(MessageSentResultCallback callback) {
        this.callbackList.add(callback);
    }

    public Queue<SubmitSmsPDU> getMessagesToBeSentQueue() {
        return messagesToBeSentQueue;
    }

    public void addModemMessageSentStat() {
        hitCount++;
    }

    public Long getHitCount() {
        return hitCount;
    }

    private class MessageSenderThread implements Runnable {

        private ModemInstance instance;

        public MessageSenderThread(ModemInstance instance) {
            this.instance = instance;
        }

        @Override
        public void run() {
            Boolean lock = false;
            while (true) {
                if (!messagesToBeSentQueue.isEmpty() && !lock) {
                    lock = true;
                    logger.debug("-----------------------__>sending smsm from queue queue size " + messagesToBeSentQueue.size());
                    SubmitSmsPDU pdu = messagesToBeSentQueue.poll();
                    ATCOutCMGS output;
                    logger.debug("-----------------------__>sending smsm from queue queue size " + messagesToBeSentQueue.size());
                    logger.debug("sending sms data " + Utils.toHexString(pdu.getPayload()));
                    try {

                        output = (ATCOutCMGS) modem.sendSmsPduMode(123, pdu.getPayload(), pdu.getDestinationNumber());
                        logger.debug("received output from sendSmsPduMethod");
                        for (MessageSentResultCallback messageSentResultCallback : callbackList) {
                            messageSentResultCallback.messageSentResult(pdu.getMessageUUID(), output.getReferenceNumber(), MessageSentResultCallback.MessageSentResultEnum.OK, instance);

                        }
                        lock = false;
                    } catch (CommandErrorException | CommandNotSupportedException | ErrorWritingATCommandToSerialPortException | InvalidAtCommandInputException | NoResponseReadFromSerialPortException ex) {
                        logger.error("[SMS SENDING QUEUE EXCEPTION] Unable to send sms ", ex);
                        for (MessageSentResultCallback messageSentResultCallback : callbackList) {
                            messageSentResultCallback.messageSentResult(pdu.getMessageUUID(), null, MessageSentResultCallback.MessageSentResultEnum.ERROR, instance);

                        }
                        lock = false;
                    } catch (Exception ex) {
                        logger.error("SOMETHING HAPPENED ", ex);
                    }
                    continue;
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ModemInstance.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }
}
