/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions;

/**
 *
 * @author martinhudec
 */
public class UnableToLoadModemConfiguration extends Exception {

    public UnableToLoadModemConfiguration(String string) {
        super(string);
    }

    public UnableToLoadModemConfiguration(String string, Throwable th) {
        super(string, th);
    }
}
