/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums;

/**
 *
 * @author martinhudec
 */
public enum ModemStatus {
    CONNECTED, ERROR_SERIAL_NOT_CONNECTED
}
