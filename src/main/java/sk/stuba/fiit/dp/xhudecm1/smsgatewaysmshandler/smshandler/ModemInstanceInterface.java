/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler;

import java.util.List;
import java.util.Queue;

/**
 *
 * @author martinhudec
 */
public interface ModemInstanceInterface {

    public void addMessageToSendQueue(SubmitSmsPDU pdu);

    public Queue<DeliverSmsPDU> getReceivedMessagesQueue();

    public Queue<DeliveryStatusSmsPDU> getReceivedStatusReportQueue();

    public boolean isNewMessage();

    public void addMessageSentResultCallback(MessageSentResultCallback callback);
}
