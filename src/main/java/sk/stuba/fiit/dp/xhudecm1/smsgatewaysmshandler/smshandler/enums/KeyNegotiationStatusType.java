/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.enums;

/**
 *
 * @author martinhudec
 */
public enum KeyNegotiationStatusType {
    CLIENT_REQUEST_RECEIVED,
    CLIENT_PUB_KEY_STORED,
    CLIENT_ACK_RECEIVED,
    
    SERVER_REQUEST_RECEIVED,
    SERVER_PUB_KEY_STORED,
    SERVER_ACK_RECEIVED,

    CLIENT_ACK_SENT,
    CLIENT_PUB_KEY_SENT,
    
    SERVER_PUB_KEY_SENT,

    ERROR_CLIENT_ACK_HASH_NOT_VALID,
    ERROR_SERVER_ACK_HASH_NOT_VALID,
    
    ERROR_CLIENT_SEQ_ID_NOT_VALID,
    ERROR_SERVER_SEQ_ID_NOT_VALID,
    
    ERROR_CLIENT_PUB_KEY_NOT_SENT,
    ERROR_CLIENT_ACK_NOT_SENT,
    ERROR_SERVER_PUB_KEY_NOT_SENT,
    ERROR_SERVER_ACK_NOT_SENT,

    ERROR,
    KEYS_GENERATED, 
    
    NEW_API_KEY_PREPARED,
    NEW_API_KEY_SENT,
    NEW_API_KEY_RECEIVED,
    NEW_API_KEY_ACK_SENT,
    NEW_API_KEY_ACK_RECEIVED,
    NEW_API_KEY_GENERATED,
    
    ERROR_NEW_API_KEY_NOT_SENT, 
    ERROR_NEW_API_KEY_ACK_NOT_SENT
}
