/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler.exceptions;

/**
 *
 * @author martinhudec
 */
public class UknownPduConentType extends Exception {

    public UknownPduConentType(String string) {
        super(string);
    }

    public UknownPduConentType(String string, Throwable th) {
        super(string, th);
    }
}
