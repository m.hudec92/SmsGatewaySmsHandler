/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.smsgatewaysmshandler.smshandler;

import java.lang.reflect.Array;
import org.apache.commons.lang3.ArrayUtils;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class SmsSecurePDUKey extends SmsSecurePDU {

    private KeyExchangeStageEnum keyExchangeStage;
    private final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SmsSecurePDUKey.class);
    private byte[] transmittedKey;
    private byte[] transmittedMd5;

    public SmsSecurePDUKey(Integer sequenceNumber, ContentTypeEnum contentType, CypherActiveEnum cypherActive, KeyExchangeStageEnum diffieHellmanStage, byte[] key, byte[] md5) {
        super(sequenceNumber, contentType, cypherActive, ArrayUtils.addAll(key, md5));
        this.keyExchangeStage = diffieHellmanStage;
    }

    public SmsSecurePDUKey(Integer sequenceNumber, ContentTypeEnum contentType, CypherActiveEnum cypherActive, KeyExchangeStageEnum keyExchangeStage, byte[] securePayload) {
        super(sequenceNumber, contentType, cypherActive, securePayload);
        this.keyExchangeStage = keyExchangeStage;
    }

    public SmsSecurePDUKey(byte[] payload) {
        super(payload);
    }

    @Override
    protected Integer setHeaderSpecific(Integer commonHeader) {
        Integer header = commonHeader;
        header = setBitInHeader(header, 11, 15, keyExchangeStage.getNumVal()); // dh id 
        header = setBitInHeader(header, 16, 16, 1); // PADDING
        return header;

    }

    @Override
    protected void getHeaderValuesSpecific(Integer commonHeader) {
        setKeyExchangeStage(KeyExchangeStageEnum.values()[getBitFromHeader(commonHeader, 11, 15)]);
    }

    public void setKeyExchangeStage(KeyExchangeStageEnum diffieHellmanStage) {
        this.keyExchangeStage = diffieHellmanStage;
    }

    public KeyExchangeStageEnum getKeyExchangeStage() {
        return keyExchangeStage;
    }

    public byte[] getTransmittedKeyByteArray() {
        if (transmittedKey == null) {
            transmittedKey = ArrayUtils.subarray(getPureData(), 0, 33);
        }
        return transmittedKey;
    }

    public byte[] getTransmittedMd5ByteArray() {
        if (transmittedMd5 == null) {
            transmittedMd5 = ArrayUtils.subarray(getPureData(), 33, 49);
        }
        return transmittedMd5;

    }

    public String getTransmittedKey() {
        return Utils.toHexString(getTransmittedKeyByteArray());
    }

    public String getTransmittedMd5() {
        return Utils.toHexString(getTransmittedMd5ByteArray());
    }

    @Override
    public String toString() {
        return "keyExchangeStage " + keyExchangeStage + super.toString();
    }
}
